#include "defs.h"
#include "mem.h"
#include "glist.h"
#include "syntax.h"

Datum* foldR(Fp func, Datum* init, Datum* ls){
  Datum *acum, *item, *_ls,*_lss;
  if(!isList(ls))
    return mkError("(foldR) not a list: ", ls);
  int len = length(ls);
  if (len == 0)
    return init;
  acum = init;
  _ls = ls;
  for(int i = 0; i<len; i++){
    item = head(_ls);
    if(isError(item))
      return item;
    _lss = createList(acum);
    _lss = append2List(item,_lss);
    acum = func(_lss);
    if(isError(acum))
      return acum;
    _ls = tail(_ls); 
  }
  return acum;}

Datum* map(Fp func, Datum* args){
  Datum *item;
  Datum *res = &nil;
  Datum *_ls = args;
  if(!isList(args))
    return mkError("(map) not a list: ", args);
  int len = length(args);
  if (len == 0)
    return res;
  for (int i = 0; i<len ; i++){
    item = head(_ls);
    if(isError(item))
      return item;
    item = func(item);
    if(isError(item))
      return item;
    res = append2List(item,res);
    _ls = tail(_ls);
  }
  return res;}

Datum* checkNum(Datum* num){
  Datum *res;
  if(!isType(num,"Number")){
    res = mkError("(checkNum) NAN: ", num);
    return res;
  }
  return num;}

Datum* checkNums(Datum* nums){
  Datum *res;
  if (isSExp(nums)){
    res = checkNum(nums);
    return res;
  }
  res = map(checkNum,nums);
  return res;}

Datum* toFloat(Datum* num){
  Datum *res,*tmp;
  double _num;
  tmp = (Datum*)num->content;
  if(isType(tmp,"Integer"))
    _num = *((int*)tmp->content);
  if(isType(tmp,"Float"))
    _num = *((double*)tmp->content);
  res = mkNum(mkFloat(_num));
  return res;}

Datum* toFloats(Datum* nums){
  Datum *res;
  if (isSExp(nums)){
    res = toFloat(nums);
    return res;
  }
  res = map(toFloat,nums);
  return res;}

Datum* sumBin(Datum* args){
  Datum *res,*_args, *dlar, *drar;
  double rar, lar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(sumBin) not the right length: ", args);
  _args = args;
  dlar = (Datum*)head(_args)->content;
  drar = (Datum*)head(tail(_args))->content;
  lar = *((double*)dlar->content);
  rar = *((double*)drar->content);
  lar += rar;
  res = mkNum(mkFloat(lar));
  return res;}

Datum* sum(Datum *args){
  Datum *item, *_args;
  Datum *acum = mkNum(mkFloat(0));
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  acum = foldR(sumBin,acum,_args);
  return acum;}

Datum* minusBin(Datum* args){
  Datum *res,*_args, *dlar, *drar;
  double rar, lar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(minusBin) not the right length: ",
		   args);
  _args = args;
  dlar = (Datum*)head(_args)->content;
  drar = (Datum*)head(tail(_args))->content;
  lar = *((double*)dlar->content);
  rar = *((double*)drar->content);
  lar -= rar;
  res = mkNum(mkFloat(lar));
  return res;}

Datum* substract(Datum* args){
  Datum *item, *_args;
  Datum *acum = mkNum(mkFloat(0));
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  int len = length(_args);
  if(len == 0){
    return acum;
  }
  if(len == 1){
    acum = createList(acum);
    acum = concatList(acum,_args);
    acum = minusBin(acum);
    return acum;
  }
  if(len >= 2){
    acum = head(_args);
    _args = tail(_args);
    acum = foldR(minusBin,acum,_args);
  }
  return acum;}

Datum* byBin(Datum* args){
  Datum *res,*_args, *dlar, *drar;
  double rar, lar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(byBin) not the right length: ", args);
  _args = args;
  dlar = (Datum*)head(_args)->content;
  drar = (Datum*)head(tail(_args))->content;
  lar = *((double*)dlar->content);
  rar = *((double*)drar->content);
  lar *= rar;
  res = mkNum(mkFloat(lar));
  return res;}

Datum* product(Datum*args){
  Datum *item, *_args;
  Datum *acum = mkNum(mkFloat(1));
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  acum = foldR(byBin,acum,_args);
  return acum;}

Datum* divBin(Datum* args){
  Datum *res,*_args, *dlar, *drar;
  double rar, lar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(divBin) not the right length: ", args);
  _args = args;
  dlar = (Datum*)head(_args)->content;
  drar = (Datum*)head(tail(_args))->content;
  lar = *((double*)dlar->content);
  rar = *((double*)drar->content);
  if (rar == 0)
    return mkError("(divBin) Div by 0: ",args);
  lar = lar / rar;
  res = mkNum(mkFloat(lar));
  return res;}

Datum* div(Datum* args){
  Datum *item, *_args;
  Datum *acum = mkNum(mkFloat(1));
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  int len = length(_args);
  if(len == 0){
    return acum;
  }
  if(len == 1){
    acum = createList(acum);
    acum = concatList(acum,_args);
    acum = divBin(acum);
    return acum;
  }
  if(len >= 2){
    acum = head(_args);
    _args = tail(_args);
    acum = foldR(divBin,acum,_args);
  }
  return acum;}

Datum* equalNumBin(Datum* args){
  Datum *res = mkError("(equalNumBin) not equal: ", args); 
  Datum *dlar, *drar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(equalNumBin) not the right length (2): ", args);
  dlar = head(args);
  drar = head(tail(args));
  if (isEqual(dlar,drar))
     return dlar;
  else
    return mkError("(equalNumBin) not equal: ", args);
}

Datum* equalNum(Datum* args ){
  Datum *_args, *res;
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  int len = length(args);
  if (len >= 2){
    Datum* init = head(_args);
    _args = tail(_args);
    res = foldR(equalNumBin,init,_args);
    if (isError(res))
      return mkFalseVal();
  }
  return mkTrueVal();
}

Datum* greaterThanBin(Datum* args){
  Datum *res = mkError("(greaterThanBin) not greater: ", args); 
  Datum *dlar, *drar;
  double rar, lar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(greaterThanBin) not the right length (2): ", args);
  dlar = (Datum*)head(args)->content;
  drar = (Datum*)head(tail(args))->content;
  lar = *((double*)dlar->content);
  rar = *((double*)drar->content);
  if (lar > rar)
     res = head(args);
  return res;
}

Datum* greaterThan(Datum* args ){
  Datum *init, *_args, *res;
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  int len = length(_args);
  if (len >= 2){
    init = head(_args);
    _args = tail(_args);
    res = foldR(greaterThanBin,init,_args);
    if (isError(res))
      return mkFalseVal();
  }
  return mkTrueVal();
}

Datum* lessThanBin(Datum* args){
  Datum *res = mkError("(lessThanBin) not less than: ", args); 
  Datum *dlar, *drar;
  double rar, lar;
  int len = length(args);
  if(!(len == 2))
    return mkError("(lessThanBin) not the right length (2): ", args);
  dlar = (Datum*)head(args)->content;
  drar = (Datum*)head(tail(args))->content;
  lar = *((double*)dlar->content);
  rar = *((double*)drar->content);
  if (lar < rar)
     res = head(args);
  return res;
}

Datum* lessThan(Datum* args ){
  Datum *init, *_args, *res;
  _args = checkNums(args);
  if (isError(_args))
    return _args;
  _args = toFloats(_args);
  int len = length(_args);
  if (len >= 2){
    init = head(_args);
    _args = tail(_args);
    res = foldR(lessThanBin,init,_args);
    if (isError(res))
      return mkFalseVal();
  }
  return mkTrueVal();
}

Datum* gOrEq(Datum* args){
  if (isTrue(greaterThan(args)) || isTrue(equalNum(args)))
    return mkTrueVal();
  return mkFalseVal();
}

Datum* lOrEq(Datum* args){
  if (isTrue(lessThan(args)) || isTrue(equalNum(args)))
    return mkTrueVal();
  return mkFalseVal();
}

Datum* pExit(Datum* nul){
  Datum *res = mkSymbol("exit");
  res = createList(res);
  res = createDatum("SExp",(void*)res);
  return res;
}

Datum* andBin(Datum* bls){
  Datum *res = mkError("(andBin) is false:\n",bls);
  Datum *fb = head(bls);
  Datum *sb = head(tail(bls));
  if (isFalse(fb))
   return res;
  if (isFalse(sb))
   return res;
  return mkTrueVal();
}

Datum* allTrue(Datum* bls){
  Datum * res = mkTrueVal();
  res = foldR(andBin, res, bls);
  if(isError(res))
    return mkFalseVal();
  return res;
}

Datum* orBin(Datum* bls){
  Datum *res = mkError("(orBin) is true:\n",bls); 
  Datum *fb = head(bls);
  Datum *sb = head(tail(bls));
  if (isTrue(fb))
    return res;
  if (isTrue(sb))
    return res;
  return mkFalseVal();
}

Datum* allFalse(Datum* bls){
  Datum * res = mkFalseVal();
  res = foldR(orBin,res,bls);
  if(isError(res))
    return mkTrueVal();
  return res;
}

Fp primitiveImplement(Datum *sexp){
  Fp res;
  res = (Fp) sexp->content;
  return res;
}

Datum* primitiveProcNames(void){
  Datum *res = createList(mkSymbol("+"));
  res = append2List(mkSymbol("-"),res);
  res = append2List(mkSymbol("*"),res);
  res = append2List(mkSymbol("/"),res);
  res = append2List(mkSymbol("="),res);
  res = append2List(mkSymbol(">"),res);
  res = append2List(mkSymbol("<"),res);
  res = append2List(mkSymbol("<="),res);
  res = append2List(mkSymbol(">="),res);
  res = append2List(mkSymbol("and"),res);
  res = append2List(mkSymbol("or"),res);
  res = append2List(mkSymbol("exit"),res);
  return res;
}

Datum* primitiveProcObjects(void){
  Datum *res = createList(mkPrimitive(sum));
  res = append2List(mkPrimitive(substract),res);
  res = append2List(mkPrimitive(product),res);
  res = append2List(mkPrimitive(div),res);
  res = append2List(mkPrimitive(equalNum),res);
  res = append2List(mkPrimitive(
				greaterThan),res);
  res = append2List(mkPrimitive(lessThan),res);
  res = append2List(mkPrimitive(lOrEq),res);
  res = append2List(mkPrimitive(gOrEq),res);
  res = append2List(mkPrimitive(allTrue),res);
  res = append2List(mkPrimitive(allFalse),res);
  res = append2List(mkPrimitive(pExit),res);
  return res;
}
