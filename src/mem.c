#include <stddef.h> // size_t
#include <gc.h>

void* new(size_t size){
  char *res;
  res = (char*)GC_MALLOC(sizeof(char)*size);
  return (void*)res;
}
