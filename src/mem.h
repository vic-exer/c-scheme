#ifndef __MEM_H
#define __MEM_H

#include <stddef.h> // size_t

extern void* new(size_t );

#endif
