#ifndef __DEFS_H_
#define __DEFS_H_
//#include <stddef.h>
#include <stdbool.h>
#define MAX_LINE 254
#define INPUT_PROMPT "scheme << "

typedef struct {
  char* type;
  void* content;
} Datum;

typedef struct ListNode {
  Datum* head;
  struct ListNode* next;
} ListNode;

typedef struct GList {
  int len;
  ListNode* actual;
  ListNode* last;
} GList;

typedef Datum*(*Fp)(Datum*);

extern char keywords[10][7];

extern bool checkPrimitive(char*);
extern bool checkBuilt(char*);
extern bool checkKeyword(char*);

extern Datum* createDatum(char*, void*);
extern Datum* mkError(char*, Datum*);
extern Datum* mkPair(Datum*,Datum*);
extern Datum* mkTrueVal(void);
extern Datum* mkFalseVal(void);

extern Datum* mkInt(int a_i);
extern Datum* mkFloat(double a_f);
extern Datum* mkNum(Datum* dat);
extern Datum* mkSymbol(char* sym);
extern Datum* mkPrimitive(Fp func);
extern Datum* mkKeyword(char* key);

#endif
