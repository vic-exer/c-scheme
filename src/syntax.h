#ifndef __SYNTAX_H_
#define __SYNTAX_H_

#include <stdbool.h>
#include "defs.h"

extern bool isList(Datum* );
extern bool isNil(Datum* );
extern bool isType(Datum*, char*);
extern bool isSelfEval(Datum* );
extern bool isVar(Datum* );
extern bool isSExp(Datum*);
extern bool isTaggedList(Datum*, char*);
extern bool isError(Datum*);
extern bool isApply(Datum*);
extern bool isEqual(Datum*,Datum*);
extern bool isPrimitive(Datum*);
extern bool isPair(Datum* );
extern bool isNull(Datum* );
extern bool noOperands(Datum* );
extern bool isQuoted(Datum* );
extern bool isKeyword(Datum* );
extern bool isDef(Datum* );
extern bool isSet(Datum* );
extern bool isIf(Datum* );
extern bool isTrue(Datum* );
extern bool isFalse(Datum* );
extern bool isLastExp(Datum* );
extern bool isBegin(Datum* );
extern bool isLambda(Datum* );
extern bool isProc(Datum* );
extern bool isCond(Datum* );
extern Datum* operator(Datum*);
extern Datum* operands(Datum*);
extern Datum* firstOp(Datum* );
extern Datum* restOps(Datum* );
extern Datum* textQuoted(Datum* );
extern Datum* defVar(Datum* );
extern Datum* defVal(Datum* );
extern Datum* getSetVar(Datum* );                          
extern Datum* getSetVal(Datum* );
extern Datum* ifPred(Datum* );
extern Datum* ifCons(Datum* );
extern Datum* ifAlt(Datum* );
extern Datum* firstExp(Datum* );
extern Datum* restExp(Datum* );
extern Datum* beginActions(Datum* );
extern Datum* makeLambda(Datum* , Datum* );
extern Datum* makeProc(Datum* , Datum* , Datum* );
extern Datum* lambdaParams(Datum* );
extern Datum* lambdaBody(Datum* );
extern Datum* procBody(Datum* );
extern Datum* procParams(Datum* );
extern Datum* procEnv(Datum* );
extern Datum* cond2If(Datum* );

#endif
