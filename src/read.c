#include <string.h>
#include "mem.h"
#include "defs.h"
#include "lex.yy.h"
#include "scheme-like.tab.h"
#include "glist.h"

Datum* t_n;

Datum* readSexp(char* src){
  yyin = fmemopen(src,strlen(src),"r");
  yyparse();
  return t_n;
}

void yyerror (char const *s){
  char *msg = (char*) new(strlen(s)+1);
  strcpy(msg,s);
  t_n = mkError(msg,&nil);
}
