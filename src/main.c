#include <stdio.h>
#include <stdbool.h> // true, false
#include <stdlib.h> // fget, exit
#include <string.h> // strcpy, strcmp
#include "defs.h"
#include "my-strings.h"
#include "mem.h"
#include "read.h"
#include "show.h"
#include "eval.h"
#include "glist.h"
#include "env.h"
#include "syntax.h"

Datum* global_env;

void checkForExit(char* str){
  if(sameString(str,"(exit)\n") || sameString(str,",q\n")){
    exit(0);
  }
}

char* evalStr(char* input){
  Datum* node = readSexp(input);
  if (!isError(node))
    node = head((Datum*) node->content);
  node = eval(node,global_env);
  char* output = showSExp(node);
  return output;
}

void runREPL(void){
  Datum* node;
  char* output;
  char * input = (char*) new(MAX_LINE+1);
  while(true){
    printf("%s",INPUT_PROMPT);
    fgets(input,MAX_LINE,stdin);
    checkForExit(input);
    output = evalStr(input);
    printf("%s\n",output);
    node = &nil;
    output = NULL;
  }
}

int main (int argc, char* argv[]){
  global_env = setupEnv();
  switch (argc){
    case 1:
      runREPL();
    case 3:
      if (sameString("-e",argv[1])){
	char* output = evalStr(argv[2]);
        printf("%s\n", output);
      }
      break;
    default:
      return 1; // something go wrong!
  }
  return 0;
}
