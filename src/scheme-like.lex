%{
#ifdef PRINT_TOK
#define TOKEN(t) printf("Token: " #t "\n")
#else
#define TOKEN(t) return (t)
#endif
#include "defs.h"
#include "scheme-like.tab.h"
%}

%option header-file="lex.yy.h"

ws          [ \t\n\r]
minus       "-"
dot3        "..."
radix_2     ("#b"|"#B")
radix_8     ("#o"|"#O")
radix_10    ("#d"|"#D")
radix_16    ("#x"|"#X")
digit_2     [01]
digit_8     [0-8]
digit_9     [1-9]
digit       [0-9]
digit_16    ({digit}|[a-f]|[A-F])
sign_digit  {minus}{digit_9}
digits      {digit}+
binary      {radix_2}{digit_2}+
octal       {radix_8}{digit_8}+
decimal     {radix_10}{natural}
hexa        {radix_16}{digit_16}+
natural     ({digit_9}|{digit_9}{digits})
integer     ([0]|{natural}|{minus}{natural})
eoE         [eE]
letter      [a-zA-Z]
one_line_comment (;.*$)
atmosphere  ({one_line_comment}|{ws})
intertoken  {atmosphere}+
sq       "'"
dq       (\")
qfslash  [\\]
qdq      ({qfslash}{dq})
qsq      ({qfslash}{sq})
any_ndq  [^\"]
any_nsq  (^\')
a_char_dq  ({qdq}|{any_ndq})
  // a_char_sq  (qsq|any_nsq)
string_dq   ({dq}{a_char_dq}*{dq})
  // string_sq   ({sq}{a_char_sq}*{sq})
  // string      ({string_dq}|{string_sq})
ext_char    [\!\%\&\*\+\-\/\:\<\=\>\?\@\$\^]
init_char   ({letter}|{ext_char})
esp_char    (\.|"+"|{minus})
sub_char    ({init_char}|{digit}|{esp_char})
peculiar_id (\+|{minus}|{dot3})
identifier  ({init_char}{sub_char}*|{peculiar_id})
true_t      "#t"
false_t     "#f"
boolean     ({true_t}|{false_t}|"true"|"false")
char_lit    (\#\\[^ \n\t\r]|"#\\space"|"#\\newline")
bra "("
ket ")"
if_t  "if"
else_t "else"
set_i  "set!"

%%
  /* {octal}    TOKEN(OCTAL); */
  /* {decimal}  TOKEN(DEC);
  /* {hexa}     TOKEN(HEX);
  /* {binary}   TOKEN(BIN);
  /* {char_lit} TOKEN(CHAR_LIT);
  /* and      TOKEN(AND_T);
  /* or       TOKEN(OR_T);
  /* list TOKEN(LIST_T); */

"."{digit_9} TOKEN(FLOAT);
"."{digits}{digit_9} TOKEN(FLOAT);
{minus}"."{digit_9} TOKEN(FLOAT);
{minus}"."{digits}{digit_9} TOKEN(FLOAT);
"0."{digit_9} TOKEN(FLOAT);
"0."{digits}{digit_9} TOKEN(FLOAT);
{minus}"0."{digit_9} TOKEN(FLOAT);
{minus}"0."{digits}{digit_9} TOKEN(FLOAT);
{sign_digit}"."{digit_9} TOKEN(FLOAT);
{sign_digit}"."{digits}{digit_9} TOKEN(FLOAT);
{digit_9}"."{digit_9} TOKEN(FLOAT);
{digit_9}"."{digits}{digit_9} TOKEN(FLOAT);
{digit_9}"."{digit_9}{eoE}{natural} TOKEN(FLOAT);
{digit_9}"."{digits}{digit_9}{eoE}{natural} TOKEN(FLOAT);
{digit_9}"."{digit_9}{eoE}{minus}{natural} TOKEN(FLOAT);
{digit_9}"."{digits}{digit_9}{eoE}{minus}{natural} TOKEN(FLOAT);
{minus}{natural}"."{digits}{digit_9} TOKEN(FLOAT);
{natural}"."{digits}{digit_9} TOKEN(FLOAT);
{sign_digit}"."{digit_9}{eoE}{natural} TOKEN(FLOAT);
{sign_digit}"."{digit_9}{eoE}{minus}{natural} TOKEN( FLOAT);
{sign_digit}"."{digits}{digit_9}{eoE}{natural} TOKEN(FLOAT);
{sign_digit}"."{digits}{digit_9}{eoE}{minus}{natural} TOKEN( FLOAT);

{integer}    TOKEN(INTEGER);

{string_dq}   TOKEN(STRING);
{bra}        TOKEN(BRA);
{ket}        TOKEN(KET);
define       TOKEN(DEFINE);
def          TOKEN(DEFINE);
{set_i}      TOKEN(SETI);
begin        TOKEN(BEGIN_T);
quote        TOKEN(QUOTE);
lambda       TOKEN(LAMBDA);
cond         TOKEN(COND);
"'"          TOKEN(QUOTE_V);
{boolean}    TOKEN(BOOL);
{if_t}       TOKEN(IF_T);
{else_t}     TOKEN(ELSE_T);
  /* "case"    TOKEN(CASE_T); */
  /* unquote   TOKEN(UNQUOTE); */

{identifier} TOKEN(ID);
{intertoken} /* eats up spaces and comments... */

.  printf( "Unrecognized pattern: %s\n", yytext);

%%
