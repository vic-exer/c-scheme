%{
#ifdef PRINT_TREE
#define PRINT_T(t) printf("%s\n",t)
#else
#define PRINT_T(t) 
#endif

#include <stdio.h>
#include <stdbool.h>
#include "mem.h"
#include "defs.h"
#include "glist.h"
#include "lex.yy.h"
#include "show.h"
#include "ast.h"

extern void yyerror(char const *s);
extern Datum* t_n;

%}
%define api.value.type {Datum*}
%token INTEGER FLOAT ID SETI BRA
%token KET BEGIN_T STRING DEFINE
%token QUOTE QUOTE_V BOOL IF_T
%token LAMBDA ELSE_T COND
  /* %token CHAR_LIT STRING EQ_GT */
  /* %token AND_T OR_T */
  /* %token  CASE_T UNQUOTE */
  /* %token LIST_T */
  /* %token BIN OCTAL DEC HEX */

%type program sexp sexps compound_exp expr assign_exp
%type value arg args

%header

%start program

%%

program :
   %empty {
     t_n = createNode("Program", &nil);
     PRINT_T(showNode(t_n));
     $$ = t_n;
   }
  | line {
    t_n = createNode("Program", $1);
    PRINT_T(showNode(t_n));
    $$ = t_n;
  }
  ;

line:
   arg {
      t_n = createList($1);
      $$ = t_n;}
  | sexps {
      $$ = $1;}
  ;

sexp :
    BRA KET {
      t_n = &nil;
      t_n = createNode("SExp",(void*) t_n);
      $$ = t_n;
    }
  | BRA expr KET {
    t_n = createNode("SExp",(void*)$2);
    $$ = t_n;
    }
  | quoted_val {
     t_n = createNode("SExp",(void*)$1);
     $$ = t_n;
    }
  | BRA args KET {
     t_n = createNode("SExp",(void*)$2);
     $$ = t_n;
    }
  ;

sexps :
   sexp sexp {
    t_n = createList($1);
    t_n = append2List($2,t_n);
    $$ = t_n;
   }
   | sexps sexp {
    t_n = append2List($2,$1);
    $$ = t_n;
   }
   ;

expr :
  assign_exp
  | def_exp
  | compound_exp
  | quoted_exp
  | if_exp
  | lambda_exp
  | cond_exp
  | arg {
    t_n = createList($1);
    $$ = t_n;}
  ;

args :
    arg arg {
    t_n = createList($1);
    t_n = append2List($2,t_n);
    $$ = t_n;
    }
  | args arg {
    t_n = append2List($2,$1);
    $$ = t_n;
    }
  ;

arg :
    value
  | id
  | sexp
  ;

assign_exp :
    DEFINE id arg {
    char* tmp_str = (char*) new(7);
    strcpy(tmp_str,"define");
    t_n = createNode("Keyword",(void*) tmp_str);
    t_n = createList(t_n);
    t_n = append2List($2,t_n);
    t_n = append2List($3,t_n);
    $$ = t_n;
    }
  | SETI id arg {
    char* tmp_str = (char*) new(5);
    strcpy(tmp_str,"set!");
    t_n = createNode("Keyword",(void*) tmp_str);
    t_n = createList(t_n);
    t_n = append2List($2,t_n);
    t_n = append2List($3,t_n);
    $$ = t_n;
    }
  ;

def_exp:
  DEFINE BRA ids KET sexp {
    char* tmp_str = (char*) new(7);
    strcpy(tmp_str,"define");
    t_n = createNode("Keyword",(void*) tmp_str);
    t_n = createList(t_n);
    Datum *tmp = createNode("SExp",(void*)$3);
    t_n = append2List(tmp,t_n);
    t_n = append2List($5,t_n);
    $$ = t_n;
  }
  ;

lambda_exp :
    LAMBDA BRA id KET sexp {
    char* tmp_str = (char*) new(7);
    strcpy(tmp_str,"lambda");
    t_n = createNode("Keyword",(void*) tmp_str);
    t_n = createList(t_n);
    Datum *tmp = createList($3);
    tmp = createNode("SExp",(void*)tmp);
    t_n = append2List(tmp,t_n);
    t_n = append2List($5,t_n);
    $$ = t_n;
    }
  | LAMBDA BRA ids KET sexp {
    char* tmp_str = (char*) new(7);
    strcpy(tmp_str,"lambda");
    t_n = createNode("Keyword",(void*) tmp_str);
    t_n = createList(t_n);
    Datum *tmp = createNode("SExp",(void*)$3);
    t_n = append2List(tmp,t_n);
    t_n = append2List($5,t_n);
    $$ = t_n;
  }
  ;

ids :
  id id {
  t_n = createList($1);
  t_n = append2List($2,t_n);
  $$ = t_n;
  }
  |
  ids id {
   t_n = append2List($2,$1);
   $$ = t_n;
  }
  ;

compound_exp :
  BEGIN_T args {
    char* tmp_str = (char*) new(6);
    strcpy(tmp_str,"begin");
    t_n = createNode("Keyword",(void*) tmp_str);
    t_n = createList(t_n);
    t_n = concatList(t_n,$2);
    $$ = t_n;
  }
  ;

quoted_exp:
    QUOTE id   {
      char* tmp_str = (char*) new(7);
      strcpy(tmp_str,"quote");
      t_n = createNode("Keyword",(void*) tmp_str);
      t_n = createList(t_n);
      t_n = append2List($2,t_n);
      $$ = t_n;
    }
  | QUOTE sexp {
      char* tmp_str = (char*) new(7);
      strcpy(tmp_str,"quote");
      t_n = createNode("Keyword",(void*) tmp_str);
      t_n = createList(t_n);
      t_n = append2List($2,t_n);
      $$ = t_n;
    }
  ;

if_exp:
    IF_T arg arg {
  t_n = createNode("Keyword",new(3));
  strcpy((char*)t_n->content,"if");
  t_n = createList(t_n);
  t_n = append2List($2,t_n);
  t_n = append2List($3,t_n);
  $$ = t_n;
  }
  | IF_T arg arg arg{
  t_n = createNode("Keyword",new(3));
  strcpy((char*)t_n->content,"if");
  t_n = createList(t_n);
  t_n = append2List($2,t_n);
  t_n = append2List($3,t_n);
  t_n = append2List($4,t_n);
  $$ = t_n;
  }
  ;

cond_exp:
    COND cond_cl{
    t_n = mkKeyword(keywords[8]);
    t_n = createList(t_n);
    t_n = append2List($2,t_n);
    $$ = t_n;
    }
  | COND cond_cls {
    t_n = mkKeyword(keywords[8]);
    t_n = createList(t_n);
    t_n = concatList(t_n,$2);
    $$ = t_n;
    }
  | COND cond_cl else_cl {
    t_n = mkKeyword(keywords[8]);
    t_n = createList(t_n);
    t_n = append2List($2,t_n);
    t_n = append2List($3,t_n);
    $$ = t_n;
    }
  | COND cond_cls else_cl {
    t_n = mkKeyword(keywords[8]);
    t_n = createList(t_n);
    t_n = concatList(t_n,$2);
    t_n = append2List($3,t_n);
    $$ = t_n;
    }
  ;

cond_cl:
    BRA arg arg KET {
    t_n = createList($2);
    t_n = append2List($3,t_n);
    t_n = createNode("SExp",(void*)t_n);
    $$ = t_n;
    }
  | BRA args arg KET {
    t_n = append2List($3,$2);
    t_n = createNode("SExp",(void*)t_n);
    $$ = t_n;
    }
  ;

cond_cls:
  cond_cl cond_cl {
    t_n = createList($1);
    t_n = append2List($2,t_n);
    $$ = t_n;}
  | cond_cls cond_cl {
    t_n = append2List($2,$1);
    $$ = t_n;}
  ;

else_cl:
    BRA ELSE_T arg KET {
      t_n = mkKeyword(keywords[9]);
      t_n = createList(t_n);
      t_n = append2List($3,t_n);
      t_n = createNode("SExp",(void*)t_n);
      $$ = t_n;
  }
  | BRA ELSE_T args KET {
      t_n = mkKeyword(keywords[9]);
      t_n = createList(t_n);
      t_n = concatList(t_n,$3);
      t_n = createNode("SExp",(void*)t_n);
      $$ = t_n;
  }
  ;

quoted_val:
    QUOTE_V id   {
      char* tmp_str = (char*) new(7);
      strcpy(tmp_str,"quote");
      t_n = createNode("Keyword",(void*) tmp_str);
      t_n = createList(t_n);
      t_n = append2List($2,t_n);
      $$ = t_n;
    }
  | QUOTE_V sexp {
      char* tmp_str = (char*) new(7);
      strcpy(tmp_str,"quote");
      t_n = createNode("Keyword",(void*) tmp_str);
      t_n = createList(t_n);
      t_n = append2List($2,t_n);
      $$ = t_n;
    }
  ;

id :
    ID  {
      int len = strlen(yytext);
      t_n = createNode("Symbol",new(len+1));
      strcpy((char*)(t_n->content),yytext);
      $$ = t_n;
    }
  ;

value :
    INTEGER {
      int *int_val = (int*) new(sizeof(int));
      sscanf(yytext,"%d", int_val);
      t_n = createNode("Integer",(void*) int_val);
      t_n = createNode("Number",(void*) t_n);
      $$ = t_n;
    }
  | FLOAT   {
      double* d_val = (double*) new(sizeof(double));
      sscanf(yytext,"%lg", d_val);
      t_n = createNode("Float",(void*) d_val);
      t_n = createNode("Number",(void*) t_n);
      $$ = t_n;
    }
  | BOOL {
      int len = strlen(yytext);
      t_n = createNode("Symbol",new(len+1));
      strcpy((char*)(t_n->content),yytext);
      $$ = t_n;
    }
 | STRING {
      int len = strlen(yytext);
      t_n = createNode("String",new(len+1));
      strcpy((char*)(t_n->content),yytext);
      $$ = t_n;
    }
    ;
%%
