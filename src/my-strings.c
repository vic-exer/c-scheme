#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "defs.h"
#include "mem.h"

bool sameString(char* str_l ,char* str_r){
  if (strcmp(str_l,str_r)==0){
    return true;
  }
  return false;
}

void cleanString(char* str_l){
  str_l[0]='\0';
}

char* prependMsg(char* pre, char* inter){
  char* res;
 int total_size = strlen(pre) + strlen(inter);
  if (total_size>=MAX_LINE)
    res = (char*) new(total_size+2);
  else
    res = (char*) new(MAX_LINE+1);
  strcpy(res,pre);
  strcat(res,inter);
  return res;
}

char* appendMsg(char* inter, char* suf){
  char* res;
  int total_size = strlen(suf) +  \
    strlen(inter);
  if (total_size>=MAX_LINE)
    res = (char*) new(total_size+2);
  else
    res = (char*) new(MAX_LINE+1);
  strcpy(res,inter);
  strcat(res,suf);
  return res;
}

char* wrapMsg(char* pre, char* inter, char* suf){
  char * res, * tmp;
  tmp = prependMsg(pre,inter);
  res = appendMsg(tmp,suf);
  return res;
}

void takeLast1(char* str){
  int len=strlen(str);
  str[len-1]='\0';
}
