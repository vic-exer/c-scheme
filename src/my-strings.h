#ifndef __MY_STR_H
#define __MY_STR_H
#include <stdbool.h>

extern bool sameString(char* , char* );
extern void cleanString(char* );

extern char* prependMsg(char* , char* );
extern char* appendMsg(char* , char* );
extern char* wrapMsg(char* , char* , char* );
extern void takeLast1(char*);

#endif
