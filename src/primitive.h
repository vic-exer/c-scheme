#ifndef __PRIM_H_
#define __PRIM_H_

#include "defs.h"

extern Fp primitiveImplement(Datum*);
extern Datum* primitiveProcNames(void);
extern Datum* primitiveProcObjects(void);

#endif
