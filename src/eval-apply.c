#include "defs.h"
#include "syntax.h"
#include "primitive.h"
#include "glist.h"
#include "env.h"
#include "show.h"

Datum* listOfVals(Datum*,Datum*);
Datum* applyPrimitiveProc(Datum*,Datum*);
Datum* evalIf(Datum*,Datum*);
Datum* evalDef(Datum*,Datum*);
Datum* evalSet(Datum*,Datum*);
Datum* evalSeq(Datum*,Datum*);

Datum* apply(Datum *proc, Datum *args){
  Datum *val = mkError("(apply) UnKnown procedure type:\n", proc);
  if(isPrimitive(proc)){
    val = applyPrimitiveProc(proc,args);
  }
  if(isProc(proc)){
    Datum *params = procParams(proc);
    Datum *body = procBody(proc);
    Datum *_env = procEnv(proc);
    Datum *env = extendEnv(params, args, _env);
    val = evalSeq(body,env);
  }
  return val;
}

Datum* eval(Datum *sexp, Datum *env){
  Datum *val = mkError("(eval) Unknown expression type:\n", sexp);
  if (isError(sexp)){
    val = sexp;
  } else
  if (isSelfEval(sexp)){
    val = sexp;
  } else
  if (isVar(sexp)){
    val = lookupVar(sexp,env);
  } else
  if (isQuoted(sexp)){
    val = textQuoted(sexp);
  } else
  if (isDef(sexp)){
    val = evalDef(sexp,env);
  } else
  if (isSet(sexp)){
    val = evalSet(sexp,env);
  } else
  if (isIf(sexp)){
    val = evalIf(sexp,env);
  } else
  if (isBegin(sexp)){
    Datum *acts = beginActions(sexp);
    val = evalSeq(acts,env);
  } else
  if (isLambda(sexp)){
    Datum* params = lambdaParams(sexp);
    Datum* body = lambdaBody(sexp);
    val = makeProc(params,body,env);
  } else
  if (isCond(sexp)){
    val = cond2If(sexp);
    val = eval(val,env);
  } else
  if (isApply(sexp)){
    Datum *res;
    Datum *op = operator(sexp);
    Datum *ons = operands(sexp);
    Datum *vals = listOfVals(ons,env);
    res = eval(op,env);
    val = apply(res,vals);
  }
  return val;
}

Datum* listOfVals(Datum *sexp, Datum *env){
  if(isError(sexp) || noOperands(sexp))
    return sexp;
  Datum* fo = firstOp(sexp);
  fo = eval(fo,env);
  if(isError(fo))
    return fo;
  Datum *fl = createList(fo);
  Datum* _rest = listOfVals(restOps(sexp),env);
  if(isError(_rest))
    return _rest;
  Datum* res = concatList(fl,_rest);
  return res;
}

Datum* evalDef( Datum* sexp, Datum* env){
  Datum *var = defVar(sexp);
  Datum *val = defVal(sexp);
  val = eval(val, env);
  setDefVar(var, val, env);
  return &nil;
}

Datum* evalSet(Datum* sexp, Datum* env){
  setVar(getSetVar(sexp), eval(getSetVal(sexp),env), env);
  return &nil;
}

Datum *evalIf(Datum *sexp, Datum* env){
  if(isTrue(eval(ifPred(sexp),env)))
    return eval(ifCons(sexp),env);
  return eval(ifAlt(sexp),env);
}

Datum * evalSeq(Datum* seq, Datum* env){
  Datum *res;
  if(isLastExp(seq)){
    res = eval(firstExp(seq), env);
    return res;}
  eval(firstExp(seq),env);
  res = evalSeq(restExp(seq),env);
  return res;
}

Datum* applyPrimitiveProc(Datum* proc, Datum* args){
  Datum* res;
  if(isError(args))
    return args;
  if(!isPrimitive(proc))
    return mkError("(applyPrimitiveProc) Not primitive proc!\n", proc);
  Fp func = primitiveImplement(proc);
  res = func(args);
  return res;
}

