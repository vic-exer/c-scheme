#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "defs.h"
#include "my-strings.h"
#include "mem.h"

bool checkNodeType(char* type){
  return checkPrimitive(type) || checkBuilt(type);
}

Datum* createNode(char* type, void* dat){
  if(checkNodeType(type)){
    Datum* res = (Datum*) new(sizeof(Datum));
    res->type = type;
    res->content = dat;
    return res;
  }
  printf("Not registered Node Type!\n");
  assert(false);
}
