#ifndef __GLIST_H
#define __GLIST_H
#include "defs.h"

extern Datum nil;
extern GList empty_glist;
extern Datum* createList(Datum* );
extern Datum* append2List(Datum*, Datum*);
extern Datum* concatList(Datum*, Datum*);
extern Datum* head(Datum* );
extern Datum* tail(Datum* );
extern Datum* last(Datum* );
extern int length(Datum* );
extern void setHead(Datum*,Datum*);
extern void setLast(Datum*,Datum*);

#endif
