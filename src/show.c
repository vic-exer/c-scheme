#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "my-strings.h"
#include "syntax.h"
#include "glist.h"
#include "mem.h"

char* showListNode(Datum*);
char* showSeqNode(Datum*);
char* showList(Datum*);
char* showSeq(Datum*);
char* showSExp(Datum*);

char* showNode(Datum* node){
  char* res= "{(Show) UnKnown:[?]}";
  char* tmp;
  char* type = node->type;
  
    if (sameString(type,"Number")){
      Datum* tmp_n = (Datum*) node->content;
      res = showNode(tmp_n);
      return res;}
  else
    if (sameString(type,"Integer")){
       tmp = (char*) new(MAX_LINE+1);
       int a_int = *((int*) node->content);
       sprintf(tmp,"%d",a_int);
       a_int = strlen(tmp);
       res = (char*) new(a_int+1);
       strcpy(res,tmp);
       return res;}
  else
    if (sameString(type,"Float")){
       tmp = (char*) new(MAX_LINE+1);
       double a_fl = *((double*) node->content);
       sprintf(tmp,"%g",a_fl);
       int a_int = strlen(tmp);
       res = (char*) new(a_int+1);
       strcpy(res,tmp);
       return res;}
  else
    if (sameString(type,"Symbol")){
       int a_int = strlen((char*)node->content);
       res = (char*)new(a_int+1);
       strcpy(res,(char*)node->content);
       return res;}
  else
       if (sameString(type,"Keyword")){
       int a_int = strlen((char*)node->content);
       res = (char*) new(a_int+1);
       strcpy(res,(char*)node->content);
       return res;}
  else
       if (sameString(type,"Pair")){
       tmp = showListNode((Datum*)node->content);
       res = wrapMsg("{Pair:",tmp,"}");
       return res;}
  else
       if (sameString(type,"SExp")){
       tmp = showListNode((Datum*)node->content);
       res = wrapMsg("{SExp:",tmp,"}");
       return res;}
  else
       if (sameString(type,"Program")){
         if(node->content == &nil)
           tmp = "[]";
         else tmp = showListNode((Datum*)node->content);
         res = wrapMsg("{Program:",tmp,"}");
         return res;}
  else
       if (isType(node,"String")){
       int a_int = strlen((char*)node->content);
       res = (char*)new(a_int+1);
       strcpy(res,
         (char*)node->content);
       return res;}
  else
       if (isType(node,"List")){
       tmp = showListNode(node);
       res = wrapMsg("[",tmp,"]");
       return res;}
  return res;
}

char* showSeqNode(Datum* lst){
  char* res = "";
  char* tmp = "";
  Datum* aux;
  aux = lst;
  if (isList(lst)){
    if(isNil(lst))
       return "";
    while(!isNil(aux)){
      tmp = showNode(head(aux));
      tmp = appendMsg(tmp," ");
      res = appendMsg(res,tmp);
      aux = tail(aux);}
  }
   else {
    printf("showSeq error: not a list!");
    exit(1);
  }
  takeLast1(res);
  return res;
}

char* showListNode(Datum* lst){
  char* res = "";
  char* tmp = "";
  tmp = showSeqNode(lst);
  res = wrapMsg("[",tmp,"]");
  return res;
}

char* showSeq(Datum* lst){
  char* res = "";
  char* tmp = "";
  Datum* aux;
  aux = lst;
  if (isList(lst)){
    if(isNil(lst))
       return "";
    while(!isNil(aux)){
      tmp = showSExp(head(aux));
      tmp = appendMsg(tmp," ");
      res = appendMsg(res,tmp);
      aux = tail(aux);}
  }
   else {
    printf("showSeq error: not a list!");
    exit(1);
  }
  takeLast1(res);
  return res;
}

char* showList(Datum* lst){
  char* res = "";
  char* tmp = "";
  tmp = showSeq(lst);
  res = wrapMsg("(",tmp,")");
  return res;
}

char* showSExp(Datum* sexp){
  int len;
  char *res, *tmp;
  if (isError(sexp)){
    res = (char*) sexp->content;
    return res;
  }
  if (isType(sexp,"Program")){
    if (sexp->content==&nil){
      res = "";
      return res;
    }
    len = length((Datum*)sexp->content);
    if (len == 1){
      Datum *_head = head((Datum*)sexp->content);
      res = showSExp(_head);
      return res;
    }else{
      res = showSeq((Datum*)sexp->content);
      return res;
    }
    return res;
  }
  if (isSelfEval(sexp)){
    res = showNode(sexp);
    return res;
  }
  if (isVar(sexp)){
    res = showNode(sexp);
    return res;
  }
  if (isKeyword(sexp)){
    res = showNode(sexp);
    return res;
  }
  if (isPair(sexp)){
    tmp = showSExp(head(sexp));
    res = showSExp(last(sexp));
    tmp = prependMsg(tmp," . ");
    res = appendMsg(tmp,res);
    res = wrapMsg("(",res,")");         
    return res;
  }
  if (isPrimitive(sexp)){
    tmp = new(MAX_LINE+1);
    sprintf(tmp,"#<Primitive: %p>",sexp->content);
    res = new(strlen(tmp)+1);
    strcpy(res,tmp);
    return res;
  }
  if (isTaggedList(sexp,"Procedure")){
    Datum *tmp_n = tail(sexp);
    char *params = showSExp(head(tmp_n));
    char *body = showSExp(head(tail(tmp_n)));
    tmp = prependMsg(params," ");
    tmp = appendMsg(tmp,body);
    tmp = appendMsg(tmp," <Env..>");
    res = wrapMsg("#<Procedure ",tmp,">");
    return res;
  }
  if (isSExp(sexp)){
       res = showList((Datum*)sexp->content);
    return res;
  }
  if (isTaggedList(sexp,"define")){
       Datum* tmp_n = tail(sexp);
       tmp = showSeq(tmp_n);
       res = wrapMsg("define ",tmp,"");
    return res;
  }
  if (isTaggedList(sexp,"set!")){
    Datum* tmp_n = tail(sexp);
    tmp = showSeq(tmp_n);
    res = wrapMsg("set! ",tmp,"");
    return res;
  }
  if (isTaggedList(sexp,"begin")){
    Datum* tmp_n = tail(sexp);
    tmp = showSeq(tmp_n);
    res = wrapMsg("begin ",tmp,"");
    return res;
  }
  if (isTaggedList(sexp,"quote")){
    Datum* tmp_n = tail(sexp);
    tmp = showSeq(tmp_n);
    res = wrapMsg("quote ",tmp,"");
    return res;
  }
  if (isTaggedList(sexp,"if")){
    Datum* tmp_n = tail(sexp);
    tmp = showSeq(tmp_n);
    res = wrapMsg("if ",tmp,"");
    return res;
  }
  if (isTaggedList(sexp,"lambda")){
    Datum* tmp_n = tail(sexp);
    tmp = showSeq(tmp_n);
    res = wrapMsg("lambda ",tmp,"");
    return res;
  }
  if (isList(sexp)){
    res = showList(sexp);
    return res;
  }
  res = "<# UnKnown(showSExp): ?>";
  return res;
}
