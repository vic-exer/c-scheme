#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "defs.h"
#include "my-strings.h"
#include "mem.h"
#include "glist.h"
#include "show.h"

char primitive_types[7][10] = {"Integer", "Float", "Char", "Symbol", "Keyword", "Primitive", "Error"};
char built_types[6][8] = {"Number", "Pair", "List", "String", "SExp", "Program"};
char keywords[10][7] = {"define", "def", "list", "if", "quote", "lambda", "set!", "begin", "cond", "else"};

bool checkPrimitive(char* type){
  for (int i = 0; i < 7; i++){
    if (sameString(type,primitive_types[i])){
     return true;
    }else{
      continue;
    }}
  return false;
}

bool checkBuilt(char* type){
  for (int i = 0; i < 6; i++){
    if (sameString(type,built_types[i])){
     return true;
    }else{
      continue;
    }}
  return false;
}

bool checkKeyword(char* type){
  for (int i = 0; i < 10; i++){
    if (sameString(type,keywords[i])){
     return true;
    }else{
      continue;
    }}
  return false;
}

bool checkDatumType(char* type){
  if (checkPrimitive(type) || checkBuilt(type))
     return true;
  return false;
}

Datum* createDatum(char* type, void* dat){
  Datum* res;
  if(checkDatumType(type)){
    res = (Datum*) new(sizeof(Datum));
    res->type = type;
    res->content = dat;
    return res;
  }
  printf("Not registered data type");
  assert(false);
}

Datum *mkError(char* msg, Datum *sexp){
  char *tmp = showSExp(sexp);
  char *res;
  char *type = new(6);
  strcpy(type,"Error");
  res = prependMsg(msg,tmp);
  res = wrapMsg("#<Error: ",res,">"); 
  return createDatum(type,res);
}

Datum* mkPair(Datum* fd, Datum* sd){
  Datum *res;
  Datum *il = &nil;
  il = append2List(fd,il);
  il = append2List(sd,il);
  res = createDatum("Pair",(void*)il);
  return res;
}

Datum* mkTrueVal(void){
  char *s = new(7);
  char *t = new(3);
  Datum *res;
  strcpy(s,"Symbol");
  strcpy(t,"#t");
  res = createDatum(s,t);
  return res;
}

Datum* mkFalseVal(void){
  char *s = new(7);
  char *t = new(3);
  Datum *res;
  strcpy(s,"Symbol");
  strcpy(t,"#f");
  res = createDatum(s,t);
  return res;
}

Datum* mkInt(int a_i){
  Datum *res = new(sizeof(Datum));
  res->type = primitive_types[0];
  int *d_i = new(sizeof(int));
  res->content = (void*)d_i;
  *((int*)res->content) = a_i;
  return res;
}

Datum* mkFloat(double a_f){
  Datum *res = new(sizeof(Datum));
  double *d_f = new(sizeof(double));
  res->type = primitive_types[1];
  res->content = (void*)d_f;
  *((double*)res->content) = a_f;
 return res;
}

Datum* mkNum(Datum* num){
  Datum *res = new(sizeof(Datum));
  res->type = built_types[0];
  res->content = (void*) num;
  return res;
}

Datum* mkSymbol(char* sym){
  Datum *res = new(sizeof(Datum));
  res->type = primitive_types[3];
  res->content = (void*) sym;
  return res;
}

Datum* mkPrimitive(Fp func){
  Datum *res = new(sizeof(Datum));
  res->type = primitive_types[5];
  res->content = (void*) func;
  return res;
}

Datum* mkKeyword(char* key){
  Datum *res = new(sizeof(Datum));
  if (checkKeyword(key)){
  res->type = primitive_types[4];
  res->content = (void*) key;
  return res;}
}

