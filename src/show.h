#ifndef __SHOW_H_
#define __SHOW_H_
#include "defs.h"

extern char* showNode(Datum*);
extern char* showSExp(Datum*);

#endif
