#include <stdio.h>
#include <assert.h>
#include "defs.h"
#include "primitive.h"
#include "glist.h"
#include "syntax.h"
#include "show.h"
// protos
Datum* envLoopLookupVar(Datum *env,Datum *var);
Datum* scanLookupVar(Datum *vars, Datum *vals, Datum *var, Datum *env);
void scanDefVar(Datum*,Datum*,Datum*,Datum*,Datum*);
void envLoopSetVar(Datum*, Datum*, Datum*);
void setDefVar(Datum* , Datum* , Datum* );

Datum* mkFrame(Datum *vars, Datum *vals){
  Datum *res = mkPair(vars,vals);
  return res;
}

Datum* extendEnv(Datum* vars, Datum* vals, Datum* base_env){
  Datum *res = &nil;
  int lenvars = length(vars);
  int lenvals = length(vals);
  if (lenvars == lenvals){
    Datum* frame = mkFrame(vars,vals);
    res = append2List(frame,res);
    res = concatList(res,base_env);
    return res;
  }else{
    if(lenvars < lenvals){
      res = mkError("More vals than vars: ", vals);
      printf("%s\n",showSExp(res));
      assert(false);
    }
    else{
      res = mkError("More vars than vals: ", vars);
      printf("%s\n",showSExp(res));
      assert(false);}}}

Datum* setupEnv(void){
  Datum *init_env;
  init_env = extendEnv(primitiveProcNames(),
		       primitiveProcObjects(),&nil);
  Datum* _true = createDatum("Symbol","true");
  Datum* _false = createDatum("Symbol","false");
  setDefVar(_true,mkTrueVal(),init_env);
  setDefVar(_false,mkFalseVal(),init_env);
  return init_env;
}

Datum* frameVars(Datum* frame){
  return head(frame);
}

Datum* frameVals(Datum* frame){
  return last(frame);
}

Datum* firstFrame(Datum* env){
  return head(env);
}

Datum* innerEnv(Datum* env){
  return tail(env);
}

Datum* scanLookupVar(Datum* vars, Datum* vals, Datum* var, Datum* env){
  if(isNil(vars))
    return envLoopLookupVar(innerEnv(env), var);
  Datum* _head = head(vars);
  if(isEqual(var, _head))
    return head(vals);
  Datum* nvars = tail(vars);
  Datum* nvals = tail(vals);
  return scanLookupVar(nvars,nvals,var,env);
}

Datum* envLoopLookupVar(Datum* env, Datum* var){
 if(isNil(env))
    return mkError("Unbound var: ",var);
  Datum* frame = firstFrame(env);
  Datum* vars = frameVars(frame);
  Datum* vals = frameVals(frame);
  return scanLookupVar(vars,vals,var,env);
}

Datum* lookupVar(Datum* var, Datum* env){
  Datum* res;
  res = envLoopLookupVar(env,var); 
  return res;
}

void setDefVar(Datum *var, Datum *val, Datum *env){
  Datum *frame = firstFrame(env);
  Datum *fVars = frameVars(frame);
  Datum *fVals = frameVals(frame);
  scanDefVar(fVars,fVals,var,val,frame);
}

void addBinding2Frame(Datum* var, Datum* val, Datum* frame){
  Datum* lvar = createList(var);
  Datum* lval = createList(val);
  Datum* vars = frameVars(frame);
  Datum* vals = frameVals(frame);
  lvar = concatList(lvar,vars);
  lval = concatList(lval,vals);
  setHead(frame,lvar);
  setLast(frame,lval);
}

void scanDefVar(Datum* vars, Datum* vals,
                Datum *var, Datum *val, Datum* frame){
  if(isNil(vars))
    return addBinding2Frame(var,val,frame);
  if(isEqual(var,head(vars)))
    printf("Var already binded: %s\n",showSExp(var));
  else {
    Datum *nvars = tail(vars);
    Datum *nvals = tail(vals);
    scanDefVar(nvars,nvals,var,val,frame);
  }
}

void scanSetVar(Datum* vars, Datum* vals,                      
                Datum *var, Datum *val, Datum* env){
  if(isNil(vars))                                          
    return envLoopSetVar(var,val,innerEnv(env));
  if(isEqual(var,head(vars)))                                
    return setHead(vals,val);
  Datum *nvars = tail(vars);
  Datum *nvals = tail(vals);
  scanSetVar(nvars,nvals,var,val,env);
}

void envLoopSetVar(Datum *var, Datum *val, Datum *env){         
  if(isNil(env))                                  
    printf("%s\n", showSExp(mkError("Unbound var: ", var)));
  else{                                            
  Datum* frame = firstFrame(env);                             
  Datum* vars = frameVars(frame);                             
  Datum* vals = frameVals(frame);                             
  scanSetVar(vars,vals,var,val,env);                         
  }}

void setVar(Datum *var, Datum *val, Datum *env){                
  envLoopSetVar(var,val,env);
}
