#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "defs.h"
#include "glist.h"
#include "my-strings.h"
#include "show.h"
// protos
Datum* headSExp(Datum* );
Datum* tailSExp(Datum* );
Datum* makeLambda(Datum*,Datum*);
Datum* condPredClause(Datum*);
Datum* firstExp(Datum*);

// selectors
bool isType(Datum *dat,char* tag){
  if (sameString(dat->type,tag))
      return true;
  return false;
}

bool isSExp(Datum* dat){
  bool res;
  res = isType(dat,"SExp");
  return res;
}

bool isList(Datum* sexp){
  if(isType(sexp,"List"))
    return true;
return false;
}

bool isNil(Datum* sexp){
  if(isList(sexp))
    if(sexp->content == &empty_glist)
      return true; 
return false;
}

bool isNumber(Datum *sexp){
  if (isType(sexp,"Number")  ||
      isType(sexp,"Integer") ||
      isType(sexp, "Float"))
      return true;
  return false;
}

bool isSymbol(Datum *sexp){
  bool res;
  res = isType(sexp,"Symbol");
  return res;
}

bool isBool(Datum *sexp){
  char *tmp = (char*) sexp->content;
  if(isSymbol(sexp))
  if(sameString("#t",tmp) || sameString("#f",tmp) || sameString("true",tmp)
     || sameString("false",tmp))
    return true;
  return false;
}

bool isString(Datum* sexp){
  if(isType(sexp,"String"))
    return true;
  return false;
}

bool isSelfEval(Datum* sexp){
  if (isNumber(sexp))
    return true;
  if(isBool(sexp))
    return true;
  if (isString(sexp))
    return true;
  return false;
}

bool isKeyword(Datum *sexp){
  bool res;
  res = isType(sexp,"Keyword");
  return res;
}

bool isNull(Datum* sexp){
  if(isSExp(sexp))
  if(isNil((Datum*)sexp->content))
    return true;
  return false;
}

bool isTaggedList(Datum* sexp, char* tag){
  Datum *tmp;
  bool res;
    if (isSExp(sexp) && !isNull(sexp)){
	tmp = (Datum*) sexp->content;
        tmp = head(tmp);
    if (isKeyword(tmp) || isSymbol(tmp)){
      res = sameString((char*)tmp->content,tag);
      return res;
    }}
    if (isList(sexp) && !isNil(sexp)){
        tmp = head(sexp);
    if (isKeyword(tmp) || isSymbol(tmp)){
      res = sameString((char*)tmp->content,tag);
      return res;
    }}
  return false;
}

bool isVar(Datum *sexp){
  bool res;
  res = isSymbol(sexp);
  return res;
}

bool isError(Datum *dat){
  bool res;
  res = isType(dat,"Error");
  return res;
}

bool isPair(Datum *sexp){
 if (isType(sexp,"Pair"))
   return true;
 return false;
}

bool isPrimitive(Datum* dat){
 bool res;
 res = isType(dat,"Primitive");
 return res;
}

bool isApply(Datum *sexp){
  if(isPair(sexp))
    return true;
  if(isSExp(sexp))
    if(length((Datum*)sexp->content) >= 1)
      return true;
  return false;
}

bool isEqual(Datum* fd, Datum* sd){
 bool res;
 char *_fd = showSExp(fd);
 char *_sd = showSExp(sd);
 res = sameString(_fd,_sd);
 return res;
}

bool isIf(Datum* sexp){
  return isTaggedList(sexp,"if");
}

bool noOperands(Datum* sexp){
 bool res;
 res = isNil(sexp);
 return res;
}

bool isQuoted(Datum* sexp){
  bool res;
  if (isSExp(sexp)){
    res = isTaggedList(sexp,"quote");
    return res;
  }
  return false;
}

bool isDef(Datum* sexp){
  bool res;
  res = isTaggedList(sexp,"define");
  return res;
}

bool isSet(Datum* sexp){
  bool res;
  res = isTaggedList(sexp,"set!");
  return res;
}

bool isFalse(Datum* sexp){
if (isNull(sexp) || isEqual(sexp,mkFalseVal()) || 
    sameString((char*)sexp->content,"false"))
    return true;
  return false;
}

bool isTrue(Datum* sexp){
  return !isFalse(sexp);
}

bool isLastExp(Datum* sexp){
  char* tmp;
  if (isSExp(sexp))
    return true;
  if (isList(sexp)){
    Datum* _tail = tail(sexp);
    bool res = isNil(_tail);
    return res;}
  tmp = showSExp(mkError("(isLastExp) Can not apply to: ",sexp));
  printf("%s\n",tmp);
  assert(false);
}

bool isBegin(Datum* sexp){
  bool res;
  res = isTaggedList(sexp,"begin");
  return res;
}

bool isLambda(Datum* sexp){
  bool res;
  res = isTaggedList(sexp,"lambda");
  return res;
}

bool isProc(Datum* sexp){
  bool res;
  res = isTaggedList(sexp,"Procedure");
  return res;
}

bool isCond(Datum* sexp){
  bool res = isTaggedList(sexp,"cond");
  return res;
}

bool isCondElseClause(Datum* clause){
  Datum * tmp = condPredClause(clause);
  bool res = isEqual(tmp,mkSymbol("else"));
  return res;
}

// -- modifiers and extractors
Datum* headSExp(Datum* sexp){
  Datum *res;
  if (isSExp(sexp)){
    res = (Datum*) sexp->content;
    res = head(res);
    return res;
  }
  res = mkError("(headSExp) Can not do to:\n", sexp);
  printf("%s\n", showSExp(res));
  assert(false);
}

Datum* tailSExp(Datum* sexp){
  Datum *res;
  if (isSExp(sexp)){
    res = (Datum*) sexp->content;
    res = tail(res);
    res = createDatum("SExp", (void*)res);
    return res;
  }
  res = mkError("Can not do tailSExp to: ", sexp);
  printf("%s\n", showSExp(res));
  assert(false);
}

Datum* condPredClause(Datum* clause){
  Datum *res = head(clause);
  return res;
}

Datum* condClauses(Datum* sexp){
  Datum *res = mkError("(condClauses) not sexp or null:\n",sexp);
  if(isSExp(sexp) && !isNull(sexp)){
  res = (Datum*) sexp->content;
  res = tail(res);
  }
  return res;
}

Datum* condPredicate(Datum* clause){
  Datum *res = headSExp(clause);
}

Datum* condActions(Datum* clause){
  Datum *res = tailSExp(clause);
  res = (Datum*)res->content;
  return res;
}

Datum* makeIf(Datum* pred, Datum* conseq, Datum* alt){
  Datum *res, *ls;
  ls = &nil;
  ls = append2List(mkSymbol("if"),ls);
  ls = append2List(pred,ls);
  ls = append2List(conseq,ls);
  ls = append2List(alt,ls);
  res = createDatum("SExp",(void*) ls);
  return res;
}

Datum* makeBegin(Datum* ls){
  Datum *res;
  res = &nil;
  res = append2List(mkSymbol("begin"),ls);
  res = createDatum("SExp",(void*) res);
  return res;
}

Datum* seq2Exp(Datum* seq){
  Datum *res = seq;
  if(isNil(seq))
    ;
  else if(isLastExp(res))
    res = firstExp(res);
  else{
    res = makeBegin(res);
    res = createDatum("SExp",(void*) res);
  }
  return res;
}

Datum* expandClauses(Datum* clauses){
  Datum *res = mkFalseVal();
  Datum *tmp;
  if (isNil(clauses))
    return res;
  Datum * first = head(clauses);
  Datum * rest  = tail(clauses);
  if (isCondElseClause(first)) {
    if(isNil(rest)) {
      res = condActions(first);
      res = seq2Exp(res);
      } else
        res = mkError("(cond2If) ELSE clause is not last:\n", clauses);
      }
  else {
    Datum *pred = condPredicate(first);
    tmp = condActions(first);
    Datum *conseq = seq2Exp(tmp);
    Datum *alt = expandClauses(rest);
    res = makeIf(pred,conseq,alt);
    }
    return res;
}

Datum* cond2If(Datum* sexp){
  Datum *clauses = condClauses(sexp);
  Datum *res = expandClauses(clauses);
  return res;
}

Datum* firstExp(Datum* sexp){
  Datum* first = mkError("(firstExp) can not be applied to:\n",sexp);
  if(isList(sexp))
    first = head(sexp);
  if(isSExp(sexp))
    first = sexp;
  return first;
}

Datum* restExp(Datum* sexp){
  Datum* _t;
  _t = tail(sexp);
  return _t;
}

Datum* operator(Datum* sexp){
  Datum *res;
  res = headSExp(sexp);
  return res;
}

Datum* operands(Datum* sexp){
  Datum *res;
  if (isSExp(sexp)){
    res = (Datum*)sexp->content;
    res = tail(res);
    return res;
  }
  return mkError("Not a SExp: ",sexp);
}

Datum* firstOp(Datum *sexp){
  Datum *res;
  res = head(sexp);
  return res; 
}

Datum* restOps(Datum *sexp){
  Datum *res;
  res = tail(sexp); 
  return res;
}

Datum* textQuoted(Datum* sexp){
  Datum *res;
  res = (Datum*) sexp->content;
  res = last(res);
  return res;
}

Datum* defVar(Datum* sexp){
  Datum* res;
  res = headSExp(tailSExp(sexp));
  if(isSymbol(res))
    return res;
  else
    return headSExp(res);
}

Datum* defVal(Datum* sexp){
  Datum *res,*params,*body;
  res = headSExp(tailSExp(sexp));
  if(isSymbol(res)){
    res = headSExp(tailSExp(tailSExp(sexp)));
    return res;
  }
  else{
    params = tailSExp(headSExp(tailSExp(sexp)));
    body = headSExp(tailSExp(tailSExp(sexp)));
    res = makeLambda(params,body);
    return res;
  }
}

Datum* getSetVar(Datum* sexp){
  Datum *res = (Datum*) sexp->content;
  Datum *_cdr = tail(res);
  res  = head(_cdr);
  return res;
}
                               
Datum* getSetVal(Datum* sexp){
  Datum *res = (Datum*) sexp->content;
  Datum* _cdr = tail(tail(res));
  res  = head(_cdr);
  return res;
}

Datum* ifPred (Datum* sexp){
  Datum *res;
  res = (Datum*) sexp->content;
  res = tail(res);
  res = head(res);
  return res;
}

Datum * ifCons(Datum *sexp){
  Datum *res;
  res = (Datum*) sexp->content;
  res = tail(res);
  res = tail(res);
  res = head(res);
  return res;
}

Datum * ifAlt(Datum *sexp){
  Datum *res,*tmp;
  res = (Datum*) sexp->content;
  tmp = tail(tail(tail(res)));
  if(isError(tmp))
    return tmp;
  if(!isNil(tmp))
    return head(tmp);
  return mkFalseVal();
}

Datum* beginActions(Datum* sexp){
  Datum *res;
  res = (Datum*)sexp->content;
  res = tail(res);
  return res;
}

Datum* lambdaParams(Datum* sexp){
  Datum *_head = headSExp(tailSExp(sexp));
  _head = (Datum*)_head->content;
  return _head;
}

Datum* lambdaBody(Datum* sexp){
  Datum *ls = tailSExp(tailSExp(sexp));
  Datum *_head = headSExp(ls);
  return _head;
}

Datum* makeLambda(Datum* params, Datum* body){
  Datum* l_symbol = createDatum("Keyword","lambda");
  Datum* lambda = createList(l_symbol);
  lambda = append2List(params,lambda);
  lambda = append2List(body,lambda);
  lambda = createDatum("SExp",(void*)lambda);
  return lambda;
}

Datum* makeProc(Datum* params, Datum* body, Datum* env){
  Datum *s_proc = createDatum("Symbol",(void*)"Procedure");
  Datum *res_proc = createList(s_proc);
  res_proc = append2List(params,res_proc);
  res_proc = append2List(body,res_proc);
  res_proc = append2List(env,res_proc);
  return res_proc;
}

Datum* procParams(Datum* sexp){
  Datum *res;
  res = head(tail(sexp));
  return res;
}

Datum* procBody(Datum* sexp){
  Datum *res;
  res = head(tail(tail(sexp)));
  return res;
}

Datum* procEnv(Datum* sexp){
  Datum *res;
  res = head(tail(tail(tail(sexp))));
  return res;
}
