#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "syntax.h"
#include "mem.h"

ListNode empty_lnode = {NULL,NULL};
GList empty_glist = {0, &empty_lnode, &empty_lnode};
Datum nil = {"List", &empty_glist};

ListNode* createLnode(Datum* datum){
  ListNode *res = (ListNode*) new(sizeof(ListNode));
  res->head = datum;
  res->next = &empty_lnode;
  return res;
}

GList* creategList(Datum* datum){
  GList *res = (GList*) new(sizeof(GList));
  res->len = 1;
  res->actual = createLnode(datum);
  res->last = res->actual;
  return res;
}

Datum* createList(Datum* datum){
  GList *tmp = creategList(datum);
  Datum* res = createDatum("List",(void*)tmp);
  return res;
}

GList* append2GList(Datum* datum, GList* lst){
  ListNode* res;
  if (lst->len == 0)
    return creategList(datum);
  if (lst->len == 1){
    res = createLnode(datum);
    lst->actual->next = res;
    lst->last = res;
    lst->len = 2;
    return lst;
  }
  if (lst->len > 1){
    res = createLnode(datum);
    lst->last->next = res;
    lst->last = res; 
    lst->len = lst->len + 1;
    return lst;
  }
  printf("append2GList error: ?");
  exit(1);
}

Datum* gListHead(GList* lst){
  if (lst->len == 0){
    printf("List error: empty list!\n");
    exit(1);
  }
  else{
    return lst->actual->head;
  }
 }

GList* gListTail(GList* lst){
  GList* res = &empty_glist;
  if (lst->len == 0){
    printf("gListTail error: empty list!\n");
    exit(1);
  }
  if (lst->len == 1)
     return res;
  res = (GList*) new(sizeof(GList));
  res->len = lst->len - 1;
  res->actual = lst->actual->next;
  res->last = lst->last;
  return res;
}

Datum* head(Datum* lst){
  Datum *res;
  if(isPair(lst)){
    res = head((Datum*)lst->content);
    return res;}
  if(isList(lst)){
    res = gListHead((GList*)lst->content);
    return res;}
  res = mkError("Can not apply unless is a list or a pair!",lst);
  return res;
}

Datum* tail(Datum* lst){
  Datum* res;
  GList* tmp;
  if(isList(lst)){
    tmp = gListTail((GList*)lst->content);
    res = createDatum("List",(void*)tmp);
    return res;
   }
  res = mkError("Can not apply unless is a list!",lst);
  return res;
}

Datum* last(Datum* lst){
  Datum* res;
  GList* tmp;
  if(isPair(lst)){
    res = last((Datum*)lst->content);
    return res;
   }
  if(isList(lst)){
    tmp = (GList*)lst->content;
    res = ((ListNode*)tmp->last)->head;
    return res;
   }
  res = mkError("Can not apply unless is a List or Pair!",lst);
  return res;
}

int lengthGlist(GList* lst){
  int res;
  res = lst->len;
  return res;
}

int length(Datum* lst){
  int res=0;
  if(isNil(lst))
    return res;
  res = lengthGlist((GList*)lst->content);
  return res;
}

Datum* append2List(Datum* datum, Datum* lst){
  Datum* res;
  if(isList(lst)){
    if (isNil(lst)){
      res = createList(datum);
      return res;}
    if (length(lst) >= 1){
      append2GList(datum,(GList*)lst->content);}
      return lst;}
  printf("Type error: can only be applied to a list!\n");
  exit(1);
}

Datum* concatList(Datum* fl, Datum* sl){
  int res_l;
  GList* gfl;
  GList* gsl;
  if (isNil(fl))
     return sl;
  if (isNil(sl))
     return fl;
  res_l = length(fl) + length(sl);
  gfl = (GList*) fl->content;
  gsl = (GList*) sl->content;
  gfl->last->next = gsl->actual;
  gfl->last = gsl->last;
  gfl->len = res_l;
  return fl;
}

void setHead(Datum *pair, Datum *val){
  Datum *_head = head(pair);
  *_head = *val;
}

void setLast(Datum *pair, Datum *val){
  Datum *_last = last(pair);
  *_last = *val;
}
