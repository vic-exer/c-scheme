#ifndef __ENV_H_
#define __ENV_H_
#include "defs.h"

extern Datum* setupEnv(void);
extern Datum* lookupVar(Datum*, Datum*);
extern void setDefVar(Datum*, Datum*, Datum*);
extern void setVar(Datum* , Datum* , Datum* );
extern Datum* extendEnv(Datum* , Datum* , Datum* );

#endif
