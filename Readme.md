#-**- mode: org -**-

```{=org}
#+STARTUP: showall
```
# C-Scheme

## Description

Making a basic SCHEME interpreter in C.

## Visuals

TODO.

## Resources

Using these:

-   The book [Structure and interpretations of computer
    programs](https://web.mit.edu/6.001/6.037/sicp.pdf) .
-   The [Flex](https://github.com/westes/flex) program.
-   The [Bison](https://www.gnu.org/software/bison/) program.
-   Checking cli with [empty](https://empty.sourceforge.net/).
-   The Scheme Language
    [Report](https://www.cs.cmu.edu/Groups/AI/html/r4rs/r4rs_toc.html).
-   The test-suite is a self made with bash-scripting and core
    utilities.
-   The [Boehm-Demers-Weiser](https://www.hboehm.info/gc/) conservative
    garbage collector.

## License

MIT.

## Project Status

-   Base Definitions almost done.
-   Main driver DONE!
-   Test-suite evolving.
-   Lexer DONE!
-   Parser in DONE!.
-   Primitives \'and\' \'or\'.
