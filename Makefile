.PHONY: all clean test-main test-lexer

all: Readme.md c-scheme

Readme.md: Readme.org
	pandoc -s Readme.org -o Readme.md

c-scheme: src/main.c
	cd src; make all; cd ..

test-main:
	cd tests; ./test_cli.sh; ./test_repl.sh; cd ..

test-lexer:
	cd tests; ./test_lexer.sh; cd ..

clean:
	cd src; make clean; cd ..
