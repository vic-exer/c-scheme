#!/bin/bash

if [ "$(ls -d ../tests)" = "../tests" ]; then
  input="fixtures/in_lexer.txt"
  expect="fixtures/expect_lexer.log"
  make lexer
  ./lexer < $input > test_lexer.log
  diff $expect test_lexer.log &&
  if [ $? -eq 1 ]; then
    echo "Some tests failed!"
  else
    echo "All Tests Passed!"
  fi
else
  echo "change to the tests directory and run again!"
fi

