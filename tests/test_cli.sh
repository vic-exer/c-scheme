#!/bin/bash
f_dir="fixtures"
input="$f_dir/in_cli.txt"
if [ -x ../c-scheme ]; then
  echo -n "" >  test_cli.log
  while IFS= read -r line
  do
    ../c-scheme -e "$line" >> test_cli.log
  done < "$input"
  diff test_cli.log $f_dir/expect_cli.log
  if [ $? -eq 1 ]; then
    echo "Some Tests failed!"
  else
    echo "All Tests Passed!"
  fi
else
  echo "No build c-scheme!"
fi
