#include <stdio.h>
#include "../src/lex.yy.h"

int main( int argc, char* argv[])
{
  ++argv, --argc; /* skip over prgram name */
  if (argc > 0)
    yyin = fopen(argv[0], "r");
  else
    yyin = stdin;
  yylex();
}
