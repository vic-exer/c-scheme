#!/bin/bash
if [ "$(ls -d ../tests)" = "../tests" ]; then
  f_dir="fixtures"
  input="$f_dir/in_parser.txt"
  make parser
  echo -n "" >  test_parser.log
  while IFS= read -r line
  do
    echo "$line" | ./parser >> test_parser.log
  done < "$input"
  diff test_parser.log $f_dir/expect_parser.log
  if [ $? -eq 1 ]; then
    echo "Some Tests failed!"
  else
    echo "All Tests Passed!"
  fi
else
    echo "change to the tests directory and run again!"
fi
