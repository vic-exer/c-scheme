#!/bin/sh
empty -f -i in_.fifo -o out.fifo -L empty.log ../.build/c-scheme
empty -w -i out.fifo -o in_.fifo 'scheme <' "hola\n"
empty -w -i out.fifo -o in_.fifo 'scheme <' "mundo\n"
empty -w -i out.fifo -o in_.fifo 'scheme <' ",q\n"
diff empty.log fixtures/expect_repl.log
if [ $? -eq 1 ]; then
  echo "REPL do not follow expect!"
else
  echo "REPL OK!"
fi
rm empty.log
