#include <stdio.h>
#include "../src/defs.h"
#include "../src/scheme-like.tab.h"

Datum * t_n;

int main(int argc, char* argv[]){
  int res;
  res = yyparse ();
  return res;
}

void yyerror (char const *s){
  fprintf(stderr, "%s\n", s);
}
